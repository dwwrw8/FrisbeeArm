extends Node3D

var input_path = "res://JointSpeeds.txt"

const MAX_VAL := 4.0

@onready var Joints = [
	$Arm/BaseBody/JBase,
	$Arm/Chest/JShoulder,
	$Arm/UpperArm/JElbow
]
@onready var Disc = $Disc

# These vars are set by the setup() function
var JOINT_UPDATE_INTERVAL = 6 # number of physics updates between each target update
var Inputs = []
func setup(inputs:Array, update_interval:int):
	Inputs = inputs
	JOINT_UPDATE_INTERVAL = update_interval


var c = 0 # Update Counter
func update_joints():
	if c == -1:
		return
	elif c >= Inputs[0].size():
		throw()
		c = -1
		for j in Joints:
			j['motor/enable'] = false
	else: 
		set_targets([Inputs[0][c], Inputs[1][c], Inputs[2][c]])
		c += 1

# Sets the velocity targets for each joint to the given values
func set_targets(jointTargets:Array) -> void:
	for i in range(len(Joints)):
		Joints[i]['motor/target_velocity'] = jointTargets[i]

var process_count = 0
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta) -> void:
	
#	if Input.is_action_just_pressed("ui_accept"):
#		throw()
	
	process_count += 1
	
	if process_count >= JOINT_UPDATE_INTERVAL:
		process_count = 0
		update_joints()

func throw():
	if not get_node_or_null("Disc/DiscLock"): return
	$Disc/DiscLock.queue_free()
	$Disc.gravity_scale = 0.3

func get_fitness():
	return -Disc.position.z
