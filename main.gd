extends Node3D

var robot_res = preload("res://simulation_unit.tscn")

const MAX_VAL := 10.0 # max value of any input float
const NUM_JOINTS : int = 3 # number of joints on the robots.

#var NumRobots = 0
var InputPath = "input.txt"
var OutputPath = "output.txt"
#var JointUpdateInterval = 6 # number of physics updates between each target update
#var Inputs = []

# Called when the node enters the scene tree for the first time.
func _ready():
	
	# Configure the sim based on command line args
	configure_simulation()
	
	# Open the specified file
	var input_file = FileAccess.open(InputPath, FileAccess.READ)
	
	# Configuration by the first lines of input file
	# Number of simulation units, ie robot arms
	var NumRobots = input_file.get_line().split("#")[0].to_int()
					# Get a line, ignoring everything after a #
	if NumRobots <= 0:
		push_error("Invalid robot count given (must be greater than 0)")
		get_tree().quit(1)

	# Number of time frames in each simulation
	var NumTimes = input_file.get_line().split("#")[0].to_int()
					# Get a line, ignoring everything after a #
	if NumTimes <= 0:
		push_error("Invalid timeframe count given (must be greater than 0)")
		get_tree().quit(1)
	
	# Number of time frames in each simulation
	var JointUpdateInterval = input_file.get_line().split("#")[0].to_int()
					# Get a line, ignoring everything after a #
	if JointUpdateInterval < 0:
		push_error("Invalid timeframe count given (must be non-negative)")
		get_tree().quit(1)
	
	# Number seconds to wait after throwing before checking distance
	var ThrowDuration = input_file.get_line().split("#")[0].to_float()
					# Get a line, ignoring everything after a #
	if ThrowDuration <= 0.0:
		push_error("Invalid timeframe count given (must be greater than 0)")
		get_tree().quit(1)
	
	var input_string_array # array to hold input for processing
	var robot_inputs = []
	for i_robot in range(NumRobots):
		var joint_inputs = []
		for joint in range(NUM_JOINTS):
			# get next input line
			input_string_array = get_next_entry(input_file)
			# verify length
			if len(input_string_array) != NumTimes:
				push_error("Invalid number of time entries! expected %s, found %s" % [NumTimes,len(input_string_array)])
				get_tree().quit(1)
			
			var float_array = []
			# Parse input
			for val_str in input_string_array:
				var val = val_str.to_float()
				if abs(val) > MAX_VAL:
					push_error("Value outside allowed range! (abs val of %s). Clamping value.6" % str(MAX_VAL))
					val = clamp(val, -MAX_VAL, MAX_VAL)
				float_array.push_back(val)
			joint_inputs.push_back(PackedFloat32Array(float_array))
		robot_inputs.push_back(joint_inputs)

	
	# Now we have the data, lets instantiate the robots
	var robots = []
	for i in range(len(robot_inputs)):
		var new_bot = robot_res.instantiate()
		new_bot.setup(robot_inputs[i], JointUpdateInterval)
		new_bot.position.z = -1*i
		add_child(new_bot)
		robots.push_back(new_bot.get_path())
	
	# Wait until all the throws are done
	var wait_time:float = float(NumTimes*JointUpdateInterval)/float(Engine.physics_ticks_per_second) + ThrowDuration
	await get_tree().create_timer(wait_time).timeout
	
	# get output
	var outputs = []
	for robot in robots:
		outputs.push_back(get_node(robot).get_fitness())
	
	# write to file
	var outputFile = FileAccess.open(OutputPath, FileAccess.WRITE)
	for entry in outputs:
		outputFile.store_line("%.3f" % entry)
	outputFile.flush()
	
	# Done! 
	await get_tree().process_frame
	get_tree().quit(0)

# Get the next line of inputs from the file, ignoring blank lines
func get_next_entry(input_file: FileAccess)->PackedStringArray:
	# Error if we ended early
	if input_file.eof_reached():
		print("Unexpected end of input file!")
		get_tree().quit(1)
		assert(false, "Unexpected end of input file!")
	
	# Get next line
	var nextl = input_file.get_csv_line()
	# If empty, recurse
	if nextl[0] == "" and len(nextl) == 1:
		return get_next_entry(input_file)
	# If not, we're good
	else: 
		return nextl
	

func configure_simulation() -> void:
	var args = parse_os_args()
	
#	NumRobots = args.get('count', 0)
#	if NumRobots <= 0:
#		print("Invalid count given (must be greater than 0)")
#		get_tree().quit(1)
	
	InputPath = args.get('input-path', InputPath)
	if not FileAccess.file_exists(InputPath):
		push_error("Invalid input file")
		get_tree().quit(1)
	
	OutputPath = args.get('output-path', OutputPath)
#	if not FileAccess.file_exists(OutputPath):
#		push_error("Invalid output file")
#		get_tree().quit(1)
	
#	JointUpdateInterval = args.get('update-interval', JointUpdateInterval)
#	if JointUpdateInterval < 0:
#		print("Invalid update-interval given (must be non-negative)")
#		get_tree().quit(1)

# Read args from cmd into dict
func parse_os_args():
	var arguments = {}
	for argument in OS.get_cmdline_args():
		if argument.find("=") > -1:
			var key_value = argument.split("=")
			arguments[key_value[0].lstrip("--")] = key_value[1]
	
	return arguments
